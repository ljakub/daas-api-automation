function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
	baseUrl: 'http://digital-daas-service-om-sqa.nplabsusk8s.catmktg.com',
    createContractPath: '/daas/contractanddelivery',
    getAllContractsPath: '/daas/contractanddelivery?rows=1&offset=1&sort=id+desc'
  };

  if (env == 'dev') {
    // customize
  } else if (env == 'e2e') {
    // customize
  }
  return config;
}