@test
Feature: Verify DaaS API - Update contract

  Background: Generate data ids
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@contractId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@deliveryId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@createdDate')

    * def clientName = name_val + random_client_number
    * def clientType = clientType_val
    * def cadence = cadence_val
    * def finalIDType = finalIDType_val
    * def dataType = dataType_val
    * def deliveryLocation = deliveryLocation_val

    Given url baseUrl
    And path createContractPath
    And request read('resources/newContract.json')
    And header Accept = 'application/json'
    And method post
    Then status 200
    * def clientID = response.id

  Scenario Outline: Create new contract | update start date and end date

    * def responseToUpdate = response
    And match response.id == clientID
    And match response.clientName == clientName
    And match response.clientType == clientType
    And match response.finalIDType == finalIDType
    And match response.cadence == cadence
    And match response.dataType == dataType
    And match $response.delivery.deliveryLocation == deliveryLocation
    And match $response.delivery.endPoint == dataType_val + '_' + clientID
    And match $response.delivery.storageContainer == 'data-math'
    And match response.startDate ==  "2020-01-06 00:06:00"
    And match response.endDate ==  "2020-06-30 00:06:00"

    * set responseToUpdate.startDate =  "2020-02-25 00:06:00"
    * set responseToUpdate.endDate =  "2020-10-10 00:06:00"
    * print 'UPDATED RESPONSE: ',responseToUpdate

    Given url baseUrl
    And path createContractPath
    And request responseToUpdate
    And header Accept = 'application/json'
    And method post
    Then status 200

    And match response.startDate ==  "2020-02-25 00:06:00"
    And match response.endDate ==  "2020-10-10 00:06:00"

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | RESELLER       | WEEKLY      | the_trade_desk  | campaign_sales | s3                   |

  Scenario Outline: Create new contract | update list of upc's

    * def responseToUpdate = response
    And match response.id == clientID
    And match response.clientName == clientName
    And match response.clientType == clientType
    And match response.finalIDType == finalIDType
    And match response.cadence == cadence
    And match response.dataType == dataType
    And match $response.delivery.deliveryLocation == deliveryLocation
    And match $response.delivery.endPoint == dataType_val + '_' + clientID
    And match $response.delivery.storageContainer == 'data-math'
    And match response.upcs ==  ["4141942008", "6905582420", "4119601013", "30045018404", "1500000310", "4400004265", "3700081867", "4111620601", "7080004146", "5100027158"]

    * set responseToUpdate.upcs =  ["4141942008", "6905582420", "4119601013"]
    * print 'UPDATED RESPONSE: ',responseToUpdate

    Given url baseUrl
    And path createContractPath
    And request responseToUpdate
    And header Accept = 'application/json'
    And method post
    Then status 200

    And match response.upcs ==  ["4141942008", "6905582420", "4119601013"]

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | NON_RESELLER   | DAILY       | the_trade_desk  | universe_sales | s3                   |

  Scenario Outline: Create new contract | update delivery location

    * def responseToUpdate = response
    And match response.id == clientID
    And match response.clientName == clientName
    And match response.clientType == clientType
    And match response.finalIDType == finalIDType
    And match response.cadence == cadence
    And match response.dataType == dataType
    And match $response.delivery.id == deliveryID
    And match $response.delivery.deliveryLocation == deliveryLocation
    And match $response.delivery.endPoint == dataType_val + '_' + clientID
    And match $response.delivery.storageContainer == 'data-math'
    And match $response.delivery.initialDeliveryDate == '2020-06-09 00:06:00'

    * set responseToUpdate.delivery.deliveryLocation = 'AzureStorage'
    * set responseToUpdate.delivery.initialDeliveryDate = '2020-12-12 00:06:00'

    * print 'UPDATED RESPONSE: ',responseToUpdate

    Given url baseUrl
    And path createContractPath
    And request responseToUpdate
    And header Accept = 'application/json'
    And method post
    Then status 200

    And match $response.delivery.id == deliveryID
    And match $response.delivery.deliveryLocation ==  'AzureStorage'
    And match $response.delivery.endPoint == dataType_val + '_' + clientID
    And match $response.delivery.storageContainer == 'data-math'
    And match $response.delivery.initialDeliveryDate == '2020-12-12 00:06:00'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | NON_RESELLER   | DAILY       | beeswax         | universe_sales | s3                   |

