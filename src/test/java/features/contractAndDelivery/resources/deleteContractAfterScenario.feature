Feature: Delete contract

  Scenario: Delete contract
    * print 'Deleting contract : ' + clientID
    Given url baseUrl
    And path createContractPath + '/' + clientID
    And header Accept = 'application/json'
    And method delete
    Then status 200