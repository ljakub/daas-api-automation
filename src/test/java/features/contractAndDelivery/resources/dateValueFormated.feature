Feature: Generate unique ids

  Background: Create date value
    * def now = function(){ return java.lang.System.currentTimeMillis() }
#    * def random = function(num) { return num.toString().substr(8, 14)}
    * def random = function(){ return Math.floor(Math.random() * 1000000)}
    * def LocalDateTime = Java.type('java.time.LocalDateTime')
    * def createDate = LocalDateTime.now()+''
    * print 'createDate :', createDate

    * def getFormattedCurrentDate =
    """
    function(s, pattern) {
    var DateTimeFormatter = Java.type("java.time.format.DateTimeFormatter");
    var LocalDate = Java.type("java.time.LocalDate");
    var dtf = DateTimeFormatter.ISO_DATE_TIME;
    try {
      var fullDate = LocalDate.parse(s, dtf);
      dtf = DateTimeFormatter.ofPattern(pattern);
      return dtf.format(fullDate);
    } catch(e) {
      karate.log('*** Date parse error: ', s);
    }
    }
    """

  @contractId
  Scenario: Generate unique contract id
    * def random_client_number = random()
    * def clientID = 'C_' + getFormattedCurrentDate(createDate,'yyMMdd') + '_' + random_client_number
    * print 'My contract ID : ',clientID

  @deliveryId
  Scenario: Generate unique contract id
    * def deliveryID = 'D_' + getFormattedCurrentDate(createDate,'yyMMdd') + '_' + random()
    * print 'My delivery ID : ',deliveryID

  @createdDate
   Scenario: Create current day value in format yyyy-MM-dd
    * def createdDate = getFormattedCurrentDate(createDate,'yyyy-MM-dd') + ' 00:00:00'
    * print 'My created day value: ',createdDate

  @removeAfter
  Scenario: After scenario function
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """