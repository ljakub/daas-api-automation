Feature: Verify DaaS API - Create single contract

  Background: Generate data ids
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@contractId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@deliveryId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@createdDate')


  Scenario Outline: Create new contract | multiple values
    * def clientName = name_val + random_client_number
    * def clientType = clientType_val
    * def cadence = cadence_val
    * def finalIDType = finalIDType_val
    * def dataType = dataType_val
    * def deliveryLocation = deliveryLocation_val
    * json idType = idType_val

    Given url baseUrl
    And path createContractPath
    And request read('classpath:features/contractAndDelivery/resources/newContract.json')
    And header Accept = 'application/json'
    And method post
    Then status 200
    And match response.id == clientID
    And match response.clientName == clientName
    And match response.clientType == clientType
    And match response.finalIDType == finalIDType
    And match response.cadence == cadence
    And match response.idTypes == idType
    And match response.dataType == dataType
    And match $response.delivery.deliveryLocation == deliveryLocation
    And match $response.delivery.endPoint == dataType_val + '_' + clientID
    And match $response.delivery.storageContainer == 'data-math'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val   | idType_val                  | dataType_val   | deliveryLocation_val |
      | client_  | NON_RESELLER   | WEEKLY      | beeswax           | ["beeswax","aaid","idfa"]   | universe_sales | s3                   |
