@test
Feature: Verify DaaS API - Update contract

  Background: Generate data ids
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@contractId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@deliveryId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@createdDate')

    * def clientName = name_val + random_client_number
    * def clientType = clientType_val
    * def cadence = cadence_val
    * def finalIDType = finalIDType_val
    * def dataType = dataType_val
    * def deliveryLocation = deliveryLocation_val

    Given url baseUrl
    And path createContractPath
    And request read('resources/newContract.json')
    And header Accept = 'application/json'
    And method post
    Then status 200
    * def clientID = response.id

  Scenario Outline: Delete existing contract

    When url baseUrl
    And path createContractPath + '/' + clientID
    And header Accept = 'application/json'
    And method delete
    Then status 200

    When url baseUrl
    And path createContractPath + '/' + clientID
    And header Accept = 'application/json'
    And method get
    Then status 404
    And match response.message == 'Contract for ID '+ clientID +' not found.'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | NON_RESELLER   | MONTHLY     | households605   | universe_sales | AzureStorage         |


  Scenario Outline: Delete contract with incorrect/non-existing id

    When url baseUrl
    And path createContractPath + '/C_123456789'
    And header Accept = 'application/json'
    And method delete
    Then status 404
    And match response.message == 'Contract for ID C_123456789 not found.'

    When url baseUrl
    And path createContractPath + '/C_123456789'
    And header Accept = 'application/json'
    And method get
    Then status 404
    And match response.message == 'Contract for ID C_123456789 not found.'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | NON_RESELLER   | MONTHLY     | households605   | universe_sales | AzureStorage         |