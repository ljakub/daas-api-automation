@test
Feature: Verify DaaS API - New contract validations

  Background: Generate data ids for contract
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@contractId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@deliveryId')
    * call read('classpath:features/contractAndDelivery/resources/dateValueFormated.feature@createdDate')

    * def clientName = name_val + random_client_number
    * def clientType = clientType_val
    * def cadence = cadence_val
    * def finalIDType = finalIDType_val
    * def dataType = dataType_val
    * def deliveryLocation = deliveryLocation_val

    Given url baseUrl
    And path createContractPath
    And request read('resources/newContract.json')
    And header Accept = 'application/json'
    And method post

  @test
  Scenario Outline: Create new contract - validations | final id types
    * print 'Test for finalIDType value :',finalIDType_val
    Then status 400
    And match response.message contains 'JSON parse error: Cannot deserialize value of type `com.catalina.daas.service.daasdataservice.dto.IDType` from String \"'
    And match response.message contains finalIDType
    And match response.message contains '\": not one of the values accepted for Enum class: [the_trade_desk, masked_ids, dativa_households, households605, beeswax, amobee, liveramphhid_ids, maids, liveramp_ids, info4]'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val | dataType_val   | deliveryLocation_val |
      | client_  | RESELLER       | WEEKLY      | the-trade_desk  | campaign_sales | s3                   |
      | client_  | NON_RESELLER   | DAILY       | .               | universe_sales | AzureStorage         |
      | client_  | RESELLER       | ONE_TIME    | *****           | campaign_sales | s3                   |
      | client_  | NON_RESELLER   | MONTHLY     | 123_/*-         | universe_sales | AzureStorage         |
      | client_  | NON_RESELLER   | WEEKLY      | ___///__???     | universe_sales | s3                   |
      | client_  | RESELLER       | ONE_TIME    | amobeeee        | campaign_sales | AzureStorage         |
      | client_  | NON_RESELLER   | DAILY       | liveramphhidids | retailer_sales | s3                   |
      | client_  | RESELLER       | ONE_TIME    | 12333maids      | campaign_sales | AzureStorage         |
      | client_  | NON_RESELLER   | MONTHLY     | liveramp-ids    | universe_sales | s3                   |
      | client_  | NON_RESELLER   | WEEKLY      | 4info4          | universe_sales | AzureStorage         |


  Scenario Outline: Create new contract - validations | cadence
    * print 'Test for cadence value :',cadence_val
    Then status 400
    And match response.message contains 'JSON parse error: Cannot deserialize value of type `com.catalina.daas.service.daasdataservice.dto.Cadence` from String \"'
    And match response.message contains cadence
    And match response.message contains '\": not one of the values accepted for Enum class: [WEEKLY, MONTHLY, ONE_TIME, DAILY]'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val   | dataType_val   | deliveryLocation_val |
      | client_  | RESELLER       | weekly      | the_trade_desk    | campaign_sales | s3                   |
      | client_  | NON_RESELLER   | daily       | masked_ids        | universe_sales | AzureStorage         |
      | client_  | RESELLER       | ONE-TIME    | dativa_households | campaign_sales | s3                   |
      | client_  | NON_RESELLER   | *****       | households605     | universe_sales | AzureStorage         |
      | client_  | NON_RESELLER   | _WEEKLY_    | beeswax           | universe_sales | s3                   |
      | client_  | RESELLER       | one_time    | amobee            | campaign_sales | AzureStorage         |
      | client_  | NON_RESELLER   | 1DAILY*     | liveramphhid_ids  | retailer_sales | s3                   |

  Scenario Outline: Create new contract - validations | data type
    * print 'Test for dataType value :',dataType_val
    Then status 400
    And match response.message contains 'JSON parse error: Cannot deserialize value of type `com.catalina.daas.service.daasdataservice.dto.DataType` from String \"'
    And match response.message contains dataType
    And match response.message contains '\": not one of the values accepted for Enum class: [universe_sales, branded_sales, campaign_sales, retailer_sales]'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val   | dataType_val     | deliveryLocation_val |
      | client_  | RESELLER       | WEEKLY      | the_trade_desk    | CAMPAIGN_SALES   | s3                   |
      | client_  | NON_RESELLER   | DAILY       | masked_ids        | Universe_Sales   | AzureStorage         |
      | client_  | RESELLER       | ONE_TIME    | dativa_households | _campaign_sales_ | s3                   |
      | client_  | NON_RESELLER   | MONTHLY     | households605     | 1retailer_sales  | AzureStorage         |
      | client_  | NON_RESELLER   | WEEKLY      | beeswax           | 888888888888888  | s3                   |
      | client_  | RESELLER       | ONE_TIME    | amobee            | ////----------   | AzureStorage         |
      | client_  | NON_RESELLER   | DAILY       | liveramphhid_ids  | ..............   | s3                   |
      | client_  | NON_RESELLER   | DAILY       | liveramphhid_ids  | Branded_sales    | s3                   |

  Scenario Outline: Create new contract - validations | delivery location
    * print 'Test for deliveryLocation value :',deliveryLocation_val
    Then status 400
    And match response.message contains 'JSON parse error: Cannot deserialize value of type `com.catalina.daas.service.daasdataservice.dto.DeliveryLocation` from String \"'
    And match response.message contains deliveryLocation
    And match response.message contains '\": not one of the values accepted for Enum class: [s3, AzureStorage]'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val   | dataType_val   | deliveryLocation_val |
      | client_  | RESELLER       | WEEKLY      | the_trade_desk    | campaign_sales | S3                   |
      | client_  | NON_RESELLER   | DAILY       | masked_ids        | universe_sales | azurestorage         |
      | client_  | RESELLER       | ONE_TIME    | dativa_households | campaign_sales | 123456               |
      | client_  | NON_RESELLER   | MONTHLY     | households605     | universe_sales | AZURESTORAGE         |
      | client_  | NON_RESELLER   | WEEKLY      | beeswax           | branded_sales  | ***                  |
      | client_  | RESELLER       | ONE_TIME    | amobee            | campaign_sales | 123_?56              |
      | client_  | NON_RESELLER   | DAILY       | liveramphhid_ids  | retailer_sales | &&&@                 |


  Scenario Outline: Create new contract - validations | client type
    * print 'Test for clientType value :',clientType_val
    Then status 400
    And match response.message contains 'JSON parse error: Cannot deserialize value of type `com.catalina.daas.service.daasdataservice.dto.ClientType` from String \"'
    And match response.message contains clientType
    And match response.message contains '\": not one of the values accepted for Enum class: [RESELLER, NON_RESELLER]'

    Examples:
      | name_val | clientType_val | cadence_val | finalIDType_val   | dataType_val   | deliveryLocation_val |
      | client_  | reseller       | WEEKLY      | the_trade_desk    | campaign_sales | s3                   |
      | client_  | non_reseller   | DAILY       | masked_ids        | universe_sales | AzureStorage         |
      | client_  | Reseller       | ONE_TIME    | dativa_households | campaign_sales | AzureStorage         |
      | client_  | ********       | MONTHLY     | households605     | universe_sales | s3                   |
      | client_  | 1258799678     | WEEKLY      | beeswax           | branded_sales  | s3                   |
      | client_  | +++___))**     | ONE_TIME    | amobee            | campaign_sales | AzureStorage         |
      | client_  | >reseller      | DAILY       | liveramphhid_ids  | retailer_sales | s3                   |