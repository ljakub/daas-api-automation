Feature: Verify DaaS API - Schema validation

  Background: Generate data ids
    * configure afterScenario =
      """
      function(){
        var info = karate.info;
        karate.log('after', info.scenarioType + ':', info.scenarioName);
        karate.call('classpath:features/contractAndDelivery/resources/deleteContractAfterScenario.feature');
      }
      """
  @test
  Scenario: Validate json-schema | contractAndDelivery

    * def result = call read('classpath:features/contractAndDelivery/resources/createSingleContract.feature')
    * def clientID = result.response.id
    * def createdDate = result.response.createdDate

    * def params = 'rows=100&offset=0&sort=createdDate+desc'
    Given url baseUrl + '/daas/contractanddelivery?'+ params
    And header Content-Type = 'application/json'
    And method get
    Then status 200

    * def contractAndDeliverySchema =
    """
    {
    "id": "#regex C_[0-9]{6}_[0-9]*",
    "clientName": "#string",
    "brandName": "#string",
    "clientType": "#regex (RESELLER|NON_RESELLER)",
    "dataType": "#regex (campaign_sales|universe_sales|retailer_sales|branded_sales)",
    "cadence": "#regex (DAILY|WEEKLY|MONTHLY|ONE_TIME)",
    "networkIds": "#array",
    "startDate": "#regex 2020-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}",
    "endDate": "#regex 2020-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}",
    "finalIDType": "#string",
    "idTypes": '#array',
    "upcs": '#[] #string',
    "brands": '#array',
    "categories": '#array',
    "deliveryId": "#regex D_[0-9]{6}_[0-9]*",
    "createdDate": "#regex 2020-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}",
    "modifiedDate": "#regex 2020-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}",
    "delivery": {
        "id": "#regex D_[0-9]{6}_[0-9]*",
        "deliveryLocation": "#regex (AzureStorage|s3)",
        "initialDeliveryDate": "#regex 2020-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}",
        "endPoint": "#regex (campaign_sales_|universe_sales_|retailer_sales_|branded_sales_)C_[0-9]{6}_[0-9]*",
        "storageContainer": "data-math"
    }
    }
    """
    * def json = response.content[0]
    * print 'JSON',json
    * match json == contractAndDeliverySchema
